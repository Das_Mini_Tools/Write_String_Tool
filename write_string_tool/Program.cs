﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Windows.Forms;

namespace write_string_tool
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length != 1 && args.Length != 3)
                error();

            if ((args.Length == 1) && (args[0].ToLower() == "-version"))
                Console.WriteLine("{0} {1}, written by {2} for catalinnc" + Environment.NewLine, Application.ProductName, Application.ProductVersion, Application.CompanyName);
            else if (args.Length == 3)
            {
                string positionStr = args[0].ToLower();
                long? position = null;

                string valueStr = args[1];
                byte[] value = null;

                string filename = args[2];

                // Let's make sure that the file exists before we proceed...
                if (!File.Exists(filename))
                    error();

                FileInfo fi = new FileInfo(filename);

                // Work out the value
                if (valueStr.ToLower().Contains("0x"))
                    value = HexToByteArray(valueStr.Substring(2));
                else if (valueStr.StartsWith("'"))
                    value = Encoding.Default.GetBytes(valueStr.Replace("'", ""));
                else
                {
                    byte tmpB;
                    ushort tmpS;
                    uint tmpI;
                    ulong tmpL;
                    BigInteger tmpBI;

                    if (byte.TryParse(valueStr, out tmpB))
                        value = new byte[] { Convert.ToByte(valueStr) };
                    else if (ushort.TryParse(valueStr, out tmpS))
                        value = BitConverter.GetBytes(tmpS);
                    else if (uint.TryParse(valueStr, out tmpI))
                        value = BitConverter.GetBytes(tmpI);
                    else if (ulong.TryParse(valueStr, out tmpL))
                        value = BitConverter.GetBytes(tmpL);
                    else if (BigInteger.TryParse(valueStr, out tmpBI))
                        value = tmpBI.ToByteArray();
                    else
                        error();

                    Array.Reverse(value);
                }
                if (value == null)
                    error();

                // Work out the position
                if (positionStr.Contains("0x"))
                    position = HexToLong(positionStr);
                else if (positionStr == "start")
                    position = 0;
                else if (positionStr == "end")
                {
                    long byteCount = 0;

                    if (valueStr.ToLower().Contains("0x"))
                    {
                        byteCount = valueStr.Substring(2).Length / 2;

                        if (valueStr.Substring(2).Length % 2 != 0)
                            byteCount++;
                    }
                    else
                    {
                        byte[] tmp = value;

                        for (int i = tmp.Length - 1; i > 0; i--)
                            if (tmp[i] != 0)
                            {
                                byteCount = ++i;
                                break;
                            }
                    }

                    position = fi.Length - byteCount;
                }
                else if (positionStr == "append")
                    position = fi.Length;
                else
                {
                    long tmp;
                    if (!long.TryParse(positionStr, out tmp))
                        error();
                    position = tmp;
                }
                if (!position.HasValue)
                    error();

                // Push the changes
                using (FileStream stream = new FileStream(filename, FileMode.Open, FileAccess.ReadWrite, FileShare.Read, 4096, FileOptions.RandomAccess))
                using (BinaryWriter writer = new BinaryWriter(stream))
                {
                    stream.Position = position.Value;

                    byte[] tmpBA = value;

                    if (tmpBA[0] == 0 && !valueStr.ToLower().Contains("0x"))
                    {
                        List<byte> tmpBAList = tmpBA.ToList();
                        tmpBAList.RemoveAt(0);
                        tmpBA = tmpBAList.ToArray();
                    }

                    long byteCount = 0;
                    for (int i = tmpBA.Length - 1; i > 0; i--)
                        if (tmpBA[i] != 0)
                        {
                            byteCount = ++i;
                            break;
                        }

                    if (byteCount == 0)
                        byteCount = 1;

                    List<byte> tmpList = new List<byte>((int)byteCount);

                    for (int i = (int)--byteCount; i > -1; i--)
                        tmpList.Add(tmpBA[i]);

                    writer.Write(tmpBA);
                }
            }
        }

        private static void error()
        {
            Console.WriteLine("{0} {1}, written by {2} for catalinnc" + Environment.NewLine, Application.ProductName, Application.ProductVersion, Application.CompanyName);

            Console.WriteLine("Usage: {0} decimal/hexadecimal_position decimal/hexadecimal_value target_file" + Environment.NewLine, Application.ProductName);

            Console.WriteLine("Valid position strings:\tappend\tend\tstart" + Environment.NewLine);

            Console.WriteLine("Examples: {0} 20 0x14 target_file", Application.ProductName);
            Console.WriteLine("\t  {0} start 50 target_file", Application.ProductName);
            Console.WriteLine("\t  {0} 0x30 2 target_file", Application.ProductName);
            Console.WriteLine("\t  {0} append 'potato' target_file", Application.ProductName);

            Environment.Exit(1);
        }

        private static BigInteger HexToBigInteger(string hexString)
        {
            BigInteger value = BigInteger.Parse(hexString.Substring(2), System.Globalization.NumberStyles.AllowHexSpecifier);

            return value;
        }

        private static long HexToLong(string hexString)
        {
            long value = long.Parse(hexString.Substring(2), System.Globalization.NumberStyles.AllowHexSpecifier);

            return value;
        }

        private static byte[] HexToByteArray(String hex)
        {
            if (hex.Length % 2 != 0)
                hex = "0" + hex;

            byte[] bytes = new byte[hex.Length / 2];
            for (int i = 0; i < hex.Length; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
    }
}
